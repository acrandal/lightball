/*
 * Reactive Light Ball
 * 
 * This Arduino source is designed to use a hardware package of:
 *  Adafruit Feather 32u4 Bluefruit LE
 *  Adafruit 9-DOF Absolute Orientation IMU Fusion Breakout - BNO055
 *  Lithium Ion Polymer Battery - 3.7v 350mAh
 *  Flora RGB Smart NeoPixel version 2 
 * 
 * These devices are soldered together and packed into a clear plastic ball for a shell.
 * The code then uses input from the accelerometer to make the LEDs light and change colors.
 * 
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
 * 
 * Copyright 2016
 * Aaron S. Crandall
 * acrandal@gmail.com
 * 
 */

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <Adafruit_NeoPixel.h>
#include <elapsedMillis.h>

// Accesing orientation board
Adafruit_BNO055 bno = Adafruit_BNO055(55);

// NeoPixel configuration and initialization
#define NeoPixelPIN    6    // GPIO pin NeoPixel bus is attached to
#define NUMPIXELS      3    // Number of NeoPixel LEDs on bus
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, NeoPixelPIN, NEO_GRB + NEO_KHZ800);


int gLEDColorState = 0;       // Current color state index (in gLEDStates)
int gLEDRGBBurstCount = 0;    // Loop variable for brightness of LED, decremented to 0 (off)
int gLEDRGBBurstSpeed = 2;    // Decrement speed for a LED burst (1 is slowest)

int gLEDColorStatesCount = 6; // How many colors are in the array
char* gLEDStatesNames[] = {"Red", "Yellow", "Green", "Cyan", "Blue", "Magenta"};
int gLEDStates[][3] =
  {
    {255,0,0},    // Red
    {255,255,0},  // Yellow
    {0,255,0},    // Green
    {0,255,255},  // Cyan
    {0,0,255},    // Blue
    {255,0,255}   // Magenta
  };
int gLEDRGB[] = {255,0,0};     // Initialize to Red for first burst
int gLEDRGBBurst[] = {0,0,0};  // Initialize to off if updated

int LEDSTATUSCHANGETIMELIMIT = 1000; // Minimum miliseconds between changes in LED color


// Higher numbers here make the ball less reactive to accelerations
// Current values are found experimentally
#define ACCELTHRESHOLD 25   // Total magnitude of linear acceleration (no gravity) to start a light burst
#define GYROTHRESHOLD 40    // Total magnitude of gyroscope acceleration (no gravity) to change LED color state


// Elapsed Milliseconds in a global
elapsedMillis gLEDStatusChangeTimeElapsed;  // Using elapsedMillis library to count miliseconds 

//****************************** SETUP ******************************************************//
// setup() -- Called by Arduino OS once at power on/reset
//
void setup(void) 
{

  gLEDColorState = 0;       // Which color to index in our arrays
  gLEDRGBBurstCount = 0;    // Initialize to zero for good behavior
  pixels.begin();           // This initializes the NeoPixel library to start LEDs
  startLEDBurst();          // Run an initial (RED) burst once ball initializes

  Serial.begin(9600);       // Set up Serial communications for debugging/output
  //while(!Serial);
  Serial.println("Initialized Burst Ball");
  Serial.println("");

  /* Initialise the orientation sensor */
  if(!bno.begin())
  {
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    // Need to tell the LEDs to go red/bad error blink instead of looping forever with no output
    while(1);
  }
  bno.setExtCrystalUse(true);
}


//******************************************** LED Stuffs *************************************//

/*
 * Change the color of the LEDs
 * 
 * Modifies the state index and updates the gLEDRGB array of RGB values to use for color bursts
 *   -- This isn't necessary... but it makes later code cleaner 
 */
void changeLEDColor()
{
  if( gLEDStatusChangeTimeElapsed  > LEDSTATUSCHANGETIMELIMIT)
  {
    gLEDColorState++;
    gLEDColorState = gLEDColorState % gLEDColorStatesCount;
    //Serial.print("Changing LED Color to: ");
    //Serial.println(gLEDStatesNames[gLEDColorState]);

    // Set Burst inital values
    gLEDRGB[0] = gLEDStates[gLEDColorState][0];
    gLEDRGB[1] = gLEDStates[gLEDColorState][1];
    gLEDRGB[2] = gLEDStates[gLEDColorState][2];

    gLEDStatusChangeTimeElapsed = 0; // reset counter to zero
  }
}

//*******************************************//
void startLEDBurst()
{
  gLEDRGBBurst[0] = gLEDRGB[0];
  gLEDRGBBurst[1] = gLEDRGB[1];
  gLEDRGBBurst[2] = gLEDRGB[2];
  gLEDRGBBurstCount = 255;
  //Serial.println("startingLEDBurst");
  updateLEDs();
}


//*******************************************//
// Update the LEDs over time //
void updateLEDs()
{
  if(gLEDRGBBurstCount >= 0){
      for(int i=0;i<NUMPIXELS;i++){
        pixels.setPixelColor(i, pixels.Color(gLEDRGBBurst[0],gLEDRGBBurst[1],gLEDRGBBurst[2]));
      }
      pixels.show();

      gLEDRGBBurstCount -= gLEDRGBBurstSpeed;
      gLEDRGBBurst[0] -= gLEDRGBBurstSpeed; if(gLEDRGBBurst[0] < 0){ gLEDRGBBurst[0] = 0; }
      gLEDRGBBurst[1] -= gLEDRGBBurstSpeed; if(gLEDRGBBurst[1] < 0){ gLEDRGBBurst[1] = 0; }
      gLEDRGBBurst[2] -= gLEDRGBBurstSpeed; if(gLEDRGBBurst[2] < 0){ gLEDRGBBurst[2] = 0; }

      if(gLEDRGBBurstCount <= 0)
      {
        for(int i=0;i<NUMPIXELS;i++)
        {
          pixels.setPixelColor(i, pixels.Color(gLEDRGBBurst[0],gLEDRGBBurst[1],gLEDRGBBurst[2]));
        }
        pixels.show();
      }
  }
}


//******************************************************//
void handleLinearLEDBurst()
{
  // Start a burst if the ball is accelerated quickly
  imu::Vector<3> lineacc = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
  float accel = abs(lineacc.x()) + abs(lineacc.y()) + abs(lineacc.z());

  if(accel > ACCELTHRESHOLD)  // Detect a impact/launch
  {
    startLEDBurst();
  }
}

//********************************************//
void handleGyroLEDStatus()
{
  // Change color and start a LED burst if spun quickly
  imu::Vector<3> gyro = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
  float gyroMagnitude = abs(gyro.x()) + abs(gyro.y()) + abs(gyro.z());

  // Detect if yaw/pitch/roll is fast "enough" to change the color.
  if(gyroMagnitude > GYROTHRESHOLD)
  {
    changeLEDColor();
    startLEDBurst();
  }
}


//**************************** MAIN LOOP **********************************************//
void loop(void) 
{
  updateLEDs();
  handleLinearLEDBurst();
  handleGyroLEDStatus();
}
